import sys
sys.path.append('.')
import torch
from torch import nn
from torchvision import transforms as T
from PIL import Image
import numpy as np
from pathlib import Path
from collections import deque
import random, datetime, os, copy
import gym
from gym.spaces import Box
from gym.wrappers import FrameStack
from nes_py.wrappers import JoypadSpace
import gym_super_mario_bros
from tensordict import TensorDict
from torchrl.data import TensorDictReplayBuffer, LazyMemmapStorage
from logs.logger import logger

class Controller:
    """
    """
    def __init__(self):
        self.logger = logger
        
        # Initialize Super Mario environment (in v0.26 change render mode to 'human' to see results on the screen)
        if gym.__version__ < '0.26':
            env = gym_super_mario_bros.make("SuperMarioBros-1-1-v0", new_step_api=True)
        else:
            env = gym_super_mario_bros.make("SuperMarioBros-1-1-v0", render_mode='rgb', apply_api_compatibility=True)
        self.logger.info(f"The system is using \"gym\" version: {gym.__version__}")
        
        # Limit the action-space to
        #   0. walk right
        #   1. jump right
        self.env = JoypadSpace(env, [["right"], ["right", "A"]])

    
if __name__ == "__main__":
    controller = Controller()
    
# CREDITS
# https://dev.to/akilesh/reinforcement-learning-in-super-mario-bros-56i9
# https://pytorch.org/tutorials/intermediate/mario_rl_tutorial.html