import logging
import yaml
from logging.handlers import RotatingFileHandler

def create_logger_object():
    # Load the YAML configuration file
    with open("config.yaml", "r") as yaml_file:
        config = yaml.safe_load(yaml_file)

    # Initializing the logger object
    logger = logging.getLogger(__name__)

    # Setting the log level
    log_level = config.get("log_level", "DEBUG") # Default to DEBUG if not specified
    logger.setLevel(getattr(logging, log_level))

    # Creating the formatters with the desired log message format
    console_formatter = logging.Formatter('%(levelname)s - %(message)s')
    file_formatter = logging.Formatter('%(asctime)s - %(levelname)s - Message: %(message)s - Path: %(filename)s - Module: %(module)s - Line Number: %(lineno)d - Function Name: %(funcName)s')

    # Creating the stream handler to log to the console
    console_handler = logging.StreamHandler()
    console_handler.setFormatter(console_formatter)
    logger.addHandler(console_handler)

    # Creating the file handler to log to a file
    file_handler = RotatingFileHandler(config["log_file"], maxBytes=config["max_log_size"], backupCount=config["max_log_files"])
    file_handler.setFormatter(file_formatter)
    logger.addHandler(file_handler)
    
    return logger

logger = create_logger_object()
