VENV := venv
TEST_SCRIPTS := $(shell find test -name '*.py')
TEST_COMMAND := python -m unittest -v
REQUIREMENTS_FILE := requirements.txt

.PHONY: default test create_venv activate_venv run_tests clean

.default: test

.create_venv:
	@python3 -m venv $(VENV) || (rm -rf $(VENV) && false)
	@$(VENV)/bin/pip install --upgrade pip
	@$(VENV)/bin/pip install -r $(REQUIREMENTS_FILE)

.activate_venv: .create_venv
	@. $(VENV)/bin/activate

.run_tests: .activate_venv
	@for script in $(TEST_SCRIPTS); do \
		echo "----------------------------------------"; \
		echo "Running tests in $$script"; \
		echo "----------------------------------------"; \
		PYTHONWARNINGS=ignore $(VENV)/bin/$(TEST_COMMAND) $$script; \
	done

# Separate target to activate environment and run tests
test: .create_venv .activate_venv .run_tests

clean:
	@rm -rf $(VENV)
