
import sys
sys.path.append('.')
import unittest
from src.controller import Controller 
from logs.logger import logger
import nes_py


class TestController(unittest.TestCase):
    def setUp(self) -> None:
        self.logger = logger

    def test_env_is_JoypadSpace_instance(self) -> None:
        """
        Checks to make sure that the controllers.env variable is a
        JoypadSpace env. This is important as we want to make sure that
        the AI only considers two movements and not the entire 256 possible
        moves that would normally be allowed by the gym enviornment.
        """
        controller = Controller()
        self.assertTrue(isinstance(controller.env, nes_py.wrappers.joypad_space.JoypadSpace))
        self.logger.info(f"The controller.env is a: {type(controller.env)} object")
    
    def test_env_is_not_none(self) -> None:
        """
        Checks to ensure that there is a env attrubute initalized
        """
        controller = Controller()
        self.assertIsNotNone(controller.env)
        
    def test_count_number_of_actions(self) -> None:
        """
        Mario should only be able to perform 2 actions. This is important
        as having too many with hurt computational time.
        """
        controller = Controller()
        number_of_actions = controller.env.action_space.n
        self.assertEqual(number_of_actions, 2)
        self.logger.info(f"The system will choose from {number_of_actions} actions")

if __name__ == "__main__":
    unittest.main()